FROM python:3.7-alpine
RUN apk add --no-cache python3-dev libstdc++ && \
  apk add --no-cache g++ && \
  apk add postgresql-dev && \
  ln -s /usr/include/locale.h /usr/include/xlocale.h
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -r requirements.txt
COPY /src/. /app/