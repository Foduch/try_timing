# try_timing


#### Требования:
- python 3.7
- pip3
- virtualenv - устанавливается через pip3 install virtualenv

#### Установка (общий принцип)

1. Клонировать репозиторий
2. Перейти в try_timing/
3. Создать новое виртуальное окружение - `virtualenv -p python3 {venvName}`
4. Активировать виртуальное окружение - 
linux: `source ./{venvName}/bin/activate`
windows: `.{venvName}\scripts\activate.bat`
5. Установить пакеты - `pip install -r requirements.txt`
6. Перейти в try_timing/src
7. Создать миграции - `python manage.py makemigrations`
8. Выполнить миграцию - `python manage.py migrate`
9. (Опционально) Создать админа - `python manage.py createsuperuser --username={username}`
10. Запустить сервер - `python manage.py runserver`

#### С использование скрипта для linux

1. Клонировать репозиторий `git clone https://gitlab.com/Foduch/try_timing.git`
2. Перейти в try_timing/ `cd try-timing`
3. Дать права на запуск скрипту `chmod +x ./scripts/install.sh`
4. Запустить скрипт `./scripts/install.sh`
5. (Опционально) Активировать виртуальное окружение и создать пользователя `python manage.py createsuperuser --username={username}`

##### Linux Mint (Ubuntu) 
- add-apt-repository ppa:deadsnakes/ppa
- apt update && apt install python3.8-venv libpython3.8-dev
- git clone --recursive https://gitlab.com/Foduch/try_timing.git
- cd try_timing
- /usr/bin/python3.8 -m venv venv3.8
- venv3.8/bin/pip3 install -r requirements.txt
- /usr/bin/python3.8 src/manage.py makemigrations
- /usr/bin/python3.8 src/manage.py migrate
-  venv3.8/bin/python src/manage.py createsuperuser --username=admin
- /usr/bin/python3.8 src/manage.py runserver

Сервер запускается на localhost:8000
Админка - /admin
Доступные апишки по /api:
- /athlete
- /athlete/{id}
- /race
- /race/{id}
- /race/{id}/points
- /race/{id}/athlete{athlete_id}
- /points
- /points/{id}

http://localhost:8000/api/athlete/
http://localhost:8000/api/athlete/{id}
http://localhost:8000/api/race
http://localhost:8000/api/race/{id}
http://localhost:8000/api/race{id}/points
http://localhost:8000/api/race{id}/athlete{athlete_id}
http://localhost:8000/api/points
http://localhost:8000/api/points/{id}