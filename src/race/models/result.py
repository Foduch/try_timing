from django.db import models
from django.core.serializers.json import DjangoJSONEncoder
import uuid
import json
import six

from .participant import ParticipantModel


class JSONField(models.TextField):
    """
    JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly.
    Django snippet #1478

    example:
        class Page(models.Model):
            data = JSONField(blank=True, null=True)


        page = Page.objects.get(pk=5)
        page.data = {'title': 'test', 'type': 3}
        page.save()
    """

    def to_python(self, value):
        if value == "":
            return None

        try:
            if isinstance(value, six.string_types):
                return json.loads(value)
        except ValueError:
            pass
        return value

    def get_db_prep_save(self, value, *args, **kwargs):
        if value == "":
            return None
        if isinstance(value, dict) or isinstance(value, list):
            value = json.dumps(value, cls=DjangoJSONEncoder)
        return super(JSONField, self).get_db_prep_save(value, *args, **kwargs)


class ResultModel(models.Model):
    '''
    Класс модели "Результат" участника.
    Содержит результат участника соревнования.
    '''
    guid        = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    participant = models.ForeignKey(ParticipantModel, on_delete=models.PROTECT)
    result_time = models.DurationField(blank=True)
    laps_count  = models.IntegerField(blank=False, default=0)
    laps_time   = models.CharField(blank=True, max_length=1024)
    status      = models.TextField(max_length=100, blank=True, default='')

    def __str__(self):
        return str(self.laps_time)

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Result'
        verbose_name_plural = 'Results'
