from django.db import models
import uuid

from constants.choices import gender
from .race_model       import RaceModel


class GroupModel(models.Model):
    '''
    Класс модели "Группа" гонки.
    Содержит данные о категории участников заезда гонки.
    '''
    guid       = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    race       = models.ForeignKey(RaceModel, on_delete=models.PROTECT)
    name       = models.CharField(max_length=100)
    gender     = models.CharField(choices=gender, max_length=20)
    age_min    = models.IntegerField(default=0, blank=False)
    age_max    = models.IntegerField(default=120, blank=False)
    laps_max   = models.IntegerField(default=1, blank=False)
    start_time = models.DateTimeField()
    # timeMax   =

    def __str__(self):
        return self.name

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Group'
        verbose_name_plural = 'Groups'
