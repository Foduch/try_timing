from django.db import models
import uuid

from .race_model import RaceModel


class TimingSpotModel(models.Model):
    '''
    Класс модели "Место фиксации результата".
    Содержит данные о точке на дистанции, в которой происходит фиксация результата.
    '''
    guid      = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name      = models.CharField(max_length=100)
    race      = models.ForeignKey(RaceModel, on_delete=models.PROTECT)
    index     = models.IntegerField(blank=False, default=1)
    is_finish = models.BooleanField(blank=False, default=True)
    is_lap    = models.BooleanField(blank=False, default=True)

    def __str__(self):
        return '{0} {1}'.format(self.name, self.race)

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Timing Spot'
        verbose_name_plural = 'Timing Spots'
