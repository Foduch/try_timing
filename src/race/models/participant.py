from django.db import models
import uuid

from athlete.models import AthleteModel
from .group         import GroupModel
from .race_model    import RaceModel


class ParticipantModel(models.Model):
    '''
    Класс модели "Участник".
    Хранит данные об участнике конкретного заезда гонки.
    '''
    guid        = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    athlete     = models.ForeignKey(AthleteModel, on_delete=models.PROTECT)
    race        = models.ForeignKey(RaceModel, on_delete=models.PROTECT)
    group       = models.ForeignKey(GroupModel, on_delete=models.PROTECT)
    number      = models.IntegerField(blank=True, default=0)
    tag         = models.CharField(max_length=50, blank=True, default='')
    description = models.CharField(max_length=1000, blank=True)

    def __str__(self):
        return '{0} {1} {2}'.format(self.athlete, self.group, self.number)

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Participant'
        verbose_name_plural = 'Participants'
