from django.db import models
import uuid


class RaceModel(models.Model):
    '''
    Класс модели "Гонка".
    Содержит основные данные соревнования.
    '''
    guid          = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name            = models.CharField(max_length=250)
    start_date_time = models.DateTimeField()
    race_type       = models.CharField(max_length=100)
    description     = models.TextField(max_length=1000, blank=True, default='')
    region          = models.TextField(max_length=100, blank=True, default='')
    city            = models.TextField(max_length=100, blank=True, default='')
    status          = models.TextField(max_length=100, blank=True, default='')
    is_deleted      = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Race'
        verbose_name_plural = 'Races'
