from django.db import models
import uuid

from .timing_spot import TimingSpotModel
from .participant import ParticipantModel


class PointModel(models.Model):
    '''
    Класс модели "Отметка" заезда.
    Содержит данные о прохождении отметки участником гонки.
    '''
    guid        = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    timing_spot = models.ForeignKey(TimingSpotModel, on_delete=models.PROTECT)
    number      = models.IntegerField()
    tag         = models.CharField(max_length=50, blank=True)
    date_time   = models.DateTimeField()

    def __str__(self):
        return '{0} {1}'.format(self.timing_spot, self.date_time)

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Point'
        verbose_name_plural = 'Points'
