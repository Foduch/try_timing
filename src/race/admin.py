from django.contrib import admin

from .models import RaceModel, PointModel, GroupModel, ParticipantModel, TimingSpotModel, ResultModel
from .forms  import RaceForm, PointForm, GroupForm, ParticipantForm, TimingSpotForm, ResultForm


# Классы для настройки отображения в панели админа.
class RaceAdmin(admin.ModelAdmin):
    list_display = ['name', 'start_date_time']
    form         = RaceForm


class PointAdmin(admin.ModelAdmin):
    list_display = ['number', 'date_time']
    form         = PointForm


class GroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'race']
    form         = GroupForm


class ParticipantAdmin(admin.ModelAdmin):
    list_display = ['athlete', 'number', 'race']
    form         = ParticipantForm


class TimingSpotAdmin(admin.ModelAdmin):
    list_display = ['name', 'race']
    form         = TimingSpotForm


class ResultAdmin(admin.ModelAdmin):
    list_display = ['participant', 'result_time', 'status']
    form         = ResultForm


admin.site.register(RaceModel, RaceAdmin)
admin.site.register(PointModel, PointAdmin)
admin.site.register(GroupModel, GroupAdmin)
admin.site.register(ParticipantModel, ParticipantAdmin)
admin.site.register(TimingSpotModel, TimingSpotAdmin)
admin.site.register(ResultModel, ResultAdmin)
