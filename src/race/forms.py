from django import forms

from race.models import GroupModel, ParticipantModel, PointModel, RaceModel, TimingSpotModel, ResultModel


class RaceForm(forms.ModelForm):
    '''
    Класс формы для валидации объекта модели при добавлении и редактирования.
    Используется в панели админа.
    '''
    class Meta:
        model  = RaceModel
        fields = [
            'name',
            'start_date_time',
            'race_type',
            'description',
            'region',
            'city',
            'status',
        ]


class PointForm(forms.ModelForm):
    class Meta:
        model  = PointModel
        fields = [
            'date_time',
            'timing_spot',
            'number',
            'tag'
        ]


class GroupForm(forms.ModelForm):
    class Meta:
        model  = GroupModel
        fields = [
            'race',
            'name',
            'gender',
            'age_min',
            'age_max',
            'laps_max',
            'start_time',
        ]


class ParticipantForm(forms.ModelForm):
    class Meta:
        model  = ParticipantModel
        fields = [
            'athlete',
            'group',
            'number',
            'tag',
            'description',
            'race'
        ]


class TimingSpotForm(forms.ModelForm):
    class Meta:
        model  = TimingSpotModel
        fields = [
            'name',
            'race',
            'index',
        ]


class ResultForm(forms.ModelForm):
    '''
    Класс формы для валидации объекта модели при добавлении и редактирования.
    Используется в панели админа.
    '''
    class Meta:
        model  = ResultModel
        fields = [
            'participant',
            'result_time',
            'laps_count',
            'laps_time',
            'status',
        ]
