from rest_framework          import generics
from rest_framework.response import Response

from race.api.serializers    import GroupSerializer
from race.models             import RaceModel, GroupModel


class GroupDetailAPIView(generics.RetrieveUpdateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = GroupSerializer
    queryset               = GroupModel.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            race_id  = self.kwargs.get('race_id')
            group_id = self.kwargs.get('group_id')
            group    = self.queryset.filter(race=race_id).get(pk=group_id)
            group_   = self.serializer_class(group)
            return Response(group_.data, status=200)
        except:
            return Response(status=400)


class GroupRaceListAPIView(generics.ListCreateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = GroupSerializer
    queryset               = GroupModel.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            race_id = self.kwargs.get('race_id')
            groups  = self.queryset.filter(race=race_id)
            objects = self.serializer_class(groups, many=True)
            return Response(objects.data, status=200)
        except:
            return Response(status=500)

    def post(self, request, *args, **kwargs):
        try:
            # race_id = self.kwargs.get('race_id')
            data = self.request.data
            # if self.request.POST:
            #     data._mutable = True
            #     data['race']  = race_id
            #     data._mutable = False
            # else:
            #     data['race']  = race_id
            serialzier_   = self.serializer_class(data=data)
            if serialzier_.is_valid():
                group = serialzier_.save()
                group = self.serializer_class(group)
                return Response(data=group.data, status=201)
            else:
                return Response(data=serialzier_.errors, status=400)
        except:
            return Response(status=500)
