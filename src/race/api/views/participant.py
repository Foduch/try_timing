from rest_framework            import generics
from rest_framework.response   import Response
from rest_framework.exceptions import APIException

from race.api.serializers    import ParticipantSerializer
from race.models             import RaceModel, ParticipantModel, GroupModel
from athlete.models          import AthleteModel
from athlete.api.serializers import AthleteSerializer


class ParticipantListAPIView(generics.ListCreateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = ParticipantSerializer
    queryset               = ParticipantModel.objects.all()
    athlete_queryset       = AthleteModel.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            race_id      = self.kwargs.get('race_id')
            participants = self.queryset.filter(race=race_id)
            serializer_  = self.serializer_class(participants, many=True)
            data         = serializer_.data
            for i in range(len(data)):
                athlete = self.athlete_queryset.get(guid=data[i]['athlete'])
                athlete_data = AthleteSerializer(athlete).data
                data[i]['athlete_data'] = athlete_data                

            return Response(data, status=200)
        except Exception as error:
            return Response(status=500, data={"message": str(error)})

    def post(self, request, *args, **kwargs):
        try:
            data    = self.request.data
            race_id = data.get('race')
            # if self.kwargs.get('race_id') != race_id:
            #     return Response(data=APIException(detail='invalid race id').get_full_details(), status=400)
            number  = data.get('number')
            tag     = data.get('tag')
            athlete = data.get('athlete')
            if (self.queryset.filter(race=race_id).filter(athlete=athlete)):
                return Response(data=APIException(detail='athlete already in race').get_full_details(), status=400)

            if (number and number != 0):
                participants = self.queryset.filter(race=race_id).filter(number=number)
                if (participants):
                    return Response(data=APIException(detail='number already in use').get_full_details(), status=400)
            else:
                if isinstance(data, dict):
                    data['number'] = 0
                else:
                    data._mutable = True
                    data['number'] = 0
                    data._mutable = False

            if (tag):
                participants = self.queryset.filter(race=race_id).filter(tag=tag)
                if (participants):
                    return Response(data=APIException(detail='tag already in use').get_full_details(), status=400)
            else:
                if isinstance(data, dict):
                    data['tag'] = ''
                else:
                    data._mutable = True
                    data['tag'] = ''
                    data._mutable = False

            serializer_   = self.serializer_class(data=data)
            if serializer_.is_valid():
                participant = serializer_.save()
                participant = self.serializer_class(participant)
                return Response(data=participant.data, status=201)
            else:
                return Response(data=serializer_.errors, status=400)
        except Exception as error:
            return Response(status=500, data={"message": str(error)})


class ParticipantDetailAPIView(generics.RetrieveUpdateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = ParticipantSerializer
    queryset               = ParticipantModel.objects.all()
    athlete_queryset       = AthleteModel.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            participant = self.queryset.get(pk=self.kwargs.get('part_id'))
            serializer_ = self.serializer_class(participant)
            data        = serializer_.data
            athlete     = self.athlete_queryset.get(pk=data['athlete'])
            athlete_data = AthleteSerializer(athlete).data
            data['athlete_data'] = athlete_data   

            return Response(data=data, status=200)
        except Exception as error:
            return Response(status=500, data={"message": str(error)})

    def put(self, request, *args, **kwargs):
        try:
            participant = self.queryset.get(pk=self.kwargs.get('part_id'))
            data        = self.request.data
            serializer_ = self.serializer_class(data=data)
            if not serializer_.is_valid():
                return Response(data=serializer_.errors, status=400)
            race_id = self.kwargs.get('race_id')
            number  = data.get('number')
            tag     = data.get('tag')

            if (number):
                participants_by_number = self.queryset.filter(race=race_id).filter(number=number)
                if (
                    (
                        len(participants_by_number) == 1 and
                        participants_by_number[0].guid == participant.guid
                    ) or
                        len(participants_by_number) == 0
                ):
                    participant.number = number
                else:
                    return Response(data=APIException(detail='number already in use').get_full_details(), status=400)
            else:
                participant.number = 0

            if (tag):
                participants_by_tag = self.queryset.filter(race=race_id).filter(tag=tag)
                if (
                    (
                        len(participants_by_tag) == 1 and
                        participants_by_tag[0].guid == participant.guid
                    ) or
                        len(participants_by_tag) == 0
                ):
                    participant.tag = tag
                else:
                    return Response(data=APIException(detail='tag already in use').get_full_details(), status=400)
            else:
                participant.tag = ''
            participant.description = data.get('description')
            participant.group       = GroupModel.objects.get(pk=data.get('group'))
            participant.save()
            serializer_ = self.serializer_class(participant)
            return Response(data=serializer_.data, status=200)

        except Exception as error:
            return Response(status=500, data={"message": str(error)})
