from rest_framework            import generics
from rest_framework.response   import Response
from rest_framework.exceptions import APIException

from race.api.serializers    import PointSerializer
from race.models             import PointModel, ParticipantModel, TimingSpotModel


class PointListAPIView(generics.ListCreateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = PointSerializer
    queryset               = PointModel.objects.all()
    participant_queryset   = ParticipantModel.objects.all()
    timing_spot_queryset   = TimingSpotModel.objects.all()

    def post(self, request, *args, **kwargs):
        race_id        = self.kwargs.get('race_id')
        data           = dict()
        data['date_time'] = self.request.data.get('date_time')
        timing_spot    = self.timing_spot_queryset.filter(race=race_id)[0]
        number         = self.request.data.get('number')
        tag            = self.request.data.get('tag')
        if number and int(number) < 1:
            return Response(
                data=APIException(detail='number must be greater or equal 1').get_full_details(),
                status=400
            )

        if number:
            participants = self.participant_queryset.filter(race=race_id).filter(number=number)
        elif tag:
            participants = self.participant_queryset.filter(race=race_id).filter(tag=tag)
        else:
            participants = list()
        if participants:
            participant = participants[0]
        else:
            return Response(
                data=APIException(detail='has no participant with that tag or number').get_full_details(),
                status=400
            )
        number         = participant.number
        participant_id = participant.guid
        tag            = participant.tag
        # Временно для упрощения ввода мест фиксации результата.
        data['timing_spot'] = str(timing_spot.pk)
        data['number']      = number
        data['tag']         = tag
        point_serializer_   = self.serializer_class(data=data)
        if (point_serializer_.is_valid()):
            point            = point_serializer_.save()
            point_serialized = self.serializer_class(point)
            return Response(data=point_serialized.data, status=201)
        else:
            return Response(data=point_serializer_.errors, status=400)
        return Response(status=500)

    def get(self, request, *args, **kwargs):
        objects = self.queryset.all()
        points_serialized = self.serializer_class(objects, many=True)
        return Response(points_serialized.data)


class PointDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = PointSerializer
    queryset               = PointModel.objects.all()

# Переделать на филтру по участнику
# class AthletePointListAPIView(generics.ListAPIView):
#     permission_classes     = []
#     authentication_classes = []
#     serializer_class       = PointSerializer
#     queryset               = PointModel.objects.all()

#     def get(self, request, *args, **kwargs):
#         race_id = self.kwargs.get('race_id')
#         athlete_id = self.kwargs.get('athlete_id')
#         objects = PointModel.objects.filter(race=race_id).filter(athlete=athlete_id)
#         serializer = PointSerializer(objects, many=True)
#         return Response(serializer.data)
