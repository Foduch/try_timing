from rest_framework          import generics
from rest_framework.response import Response

from race.models          import TimingSpotModel
from race.api.serializers import TimingSpotSerializer


class TimingSpotListAPIView(generics.ListAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = TimingSpotSerializer
    queryset               = TimingSpotModel.objects.all()

    # def post(self, request, *args, **kwargs):
    #     try:
    #         data        = self.request.data
    #         race_id     = data.get('race_id')
    #         serialzier_  = self.serializer_class(data=data)
    #         if serialzier_.is_valid():
    #             spot = serialzier_.save()
    #             spot = self.serializer_class(spot)
    #             return Response(data=spot.data, status=201)
    #         else:
    #             return Response(data=serialzier_.errors, status=400)
    #     except:
    #         return Response(status=500)

    def get(self, request, *args, **kwargs):
        try:
            race_id = self.kwargs.get('race_id')
            if not race_id:
                return Response(status=404)
            spots = self.queryset.filter(race=race_id)
            data  = self.serializer_class(spots, many=True)
            return Response(data.data, status=200)
        except:
            return Response(status=500)
