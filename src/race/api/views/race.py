from rest_framework          import generics
from rest_framework.response import Response

from race.api.serializers    import RaceSerializer, ParticipantSerializer
from race.models             import RaceModel, ParticipantModel, TimingSpotModel


class RaceDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = RaceSerializer
    queryset               = RaceModel.objects.all()


class RaceListApiView(generics.ListCreateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = RaceSerializer
    queryset               = RaceModel.objects.all()

    def post(self, request, *args, **kwargs):
        data         = self.request.data        
        serialzier_  = self.serializer_class(data=data)
        if serialzier_.is_valid():
            race            = serialzier_.save()
            race_serialized = self.serializer_class(race)
            # Временная затычка для будущего функционала мест фиксации результата.
            timing_spot = TimingSpotModel(
                name='Finish and lap spot',
                race=race,
                index=0,
                is_lap=True,
                is_finish=True
            )
            timing_spot.save()
            return Response(data=race_serialized.data, status=201)
        else:
            return Response(data=serialzier_.errors, status=400)


class RaceParticipantsListAPIView(generics.ListAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = ParticipantSerializer
    queryset               = ParticipantModel.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            race_id      = self.kwargs.get('race_id')
            participants = self.queryset.filter(race=race_id)
            serializer   = self.serializer_class(participants, many=True)
            return Response(serializer.data)
        except:
            return Response(status=400)
