from rest_framework            import generics
from rest_framework.renderers  import JSONRenderer
from rest_framework.response   import Response
from rest_framework.exceptions import APIException
from datetime                  import timedelta
import json

from race.api.serializers    import ResultSerializer
from race.models             import PointModel, ParticipantModel, ResultModel, GroupModel, RaceModel


min_lap = timedelta(minutes=2)


class ResultProcessAPIView(generics.RetrieveAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = ResultSerializer
    queryset               = ResultModel.objects.all()
    group_queryset         = GroupModel.objects.all()
    race_queryset          = RaceModel.objects.all()
    point_queryset         = PointModel.objects.all()
    participant_queryset   = ParticipantModel.objects.all()
    ResultModel            = ResultModel

    def get(self, request, *args, **kwargs):
        race_id    = self.kwargs.get('race_id')
        groups     = self.group_queryset.filter(race=race_id)
        race       = self.race_queryset.get(pk=race_id)
        start_time = race.start_date_time
        if groups:
            for group in groups:
                participants = self.participant_queryset.filter(group=group.guid)
                for participant in participants:
                    total_time = None
                    laps_count = 0
                    laps_time  = ''
                    laps       = list()
                    result     = None
                    process_points = [start_time, ]
                    points     = self.point_queryset.filter(number=participant.number)
                    if points:
                        for point in points:
                            if point.date_time - process_points[-1] > min_lap:
                                process_points.append(point.date_time)
                        if len(process_points) > 1:
                            for i in range(1, len(process_points)):
                                laps.append(process_points[i] - process_points[i - 1])
                        laps_count     = len(laps)
                        total_time     = process_points[-1] - process_points[0]
                        stored_results = self.queryset.filter(participant=participant.guid)
                        laps_time      = [str(lap) for lap in laps]
                        if stored_results:
                            result             = stored_results[0]
                            result.result_time = total_time
                            result.laps_count  = laps_count
                            result.laps_time   = json.dumps(laps_time)
                        else:
                            result = self.ResultModel(
                                participant=participant,
                                result_time=total_time,
                                laps_count=laps_count,
                                laps_time=json.dumps(laps_time)
                            )
                        result.save()
        else:
            return Response(data=APIException(detail='has no groups in race').get_full_details(), status=400)
        return Response(data={'status': 'processed'}, status=200)


class ResultListRetrieveAPIView(generics.ListAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = ResultSerializer
    queryset               = ResultModel.objects.all()
    group_queryset         = GroupModel.objects.all()
    participant_queryset   = ParticipantModel.objects.all()

    def get(self, request, *args, **kwargs):
        race_id = self.kwargs.get('race_id')
        groups  = self.group_queryset.filter(race=race_id)
        results = dict()
        for group in groups:
            participants        = self.participant_queryset.filter(group=group.guid)
            participants_guids  = [part.guid for part in participants]
            group_results        = self.queryset.filter(participant__in=participants_guids).order_by('-laps_count', 'result_time')
            serialized_result   = self.serializer_class(group_results, many=True)
            results[str(group)] = serialized_result.data
        return Response(data=results, status=200)
