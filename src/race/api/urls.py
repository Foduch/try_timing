from django.urls import path

from .views import (
    RaceDetailAPIView,
    RaceListApiView,
    PointListAPIView,
    PointDetailAPIView,
    ResultProcessAPIView,
    ResultListRetrieveAPIView,
)
from race.api.views.group       import GroupRaceListAPIView, GroupDetailAPIView
from race.api.views.timing_spot import TimingSpotListAPIView
from race.api.views.participant import ParticipantListAPIView, ParticipantDetailAPIView

urlpatterns = [
    path('point/<uuid:pk>', PointDetailAPIView.as_view()),
    path('<uuid:race_id>/participant/', ParticipantListAPIView.as_view()),
    path('<uuid:race_id>/participant/<uuid:part_id>', ParticipantDetailAPIView.as_view()),
    path('<uuid:race_id>/point/', PointListAPIView.as_view()),
    path('<uuid:race_id>/group/', GroupRaceListAPIView.as_view()),
    path('<uuid:race_id>/group/<uuid:group_id>', GroupDetailAPIView.as_view()),
    path('<uuid:race_id>/timing_spot/', TimingSpotListAPIView.as_view()),
    path('<uuid:race_id>/result/process', ResultProcessAPIView.as_view()),
    path('<uuid:race_id>/result/', ResultListRetrieveAPIView.as_view()),
    path('<uuid:pk>', RaceDetailAPIView.as_view()),
    path('', RaceListApiView.as_view()),
]
