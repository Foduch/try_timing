from rest_framework import serializers

from race.models import GroupModel


class GroupSerializer(serializers.ModelSerializer):
    '''
    Класс сериализатора для api.
    '''
    class Meta:
        model  = GroupModel
        fields = [
            'guid',
            'race',
            'name',
            'gender',
            'age_min',
            'age_max',
            'laps_max',
            'start_time',
        ]
