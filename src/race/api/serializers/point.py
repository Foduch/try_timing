from rest_framework import serializers

from race.models import PointModel


class PointSerializer(serializers.ModelSerializer):
    class Meta:
        model = PointModel
        fields = [
            'guid',
            'date_time',
            'timing_spot',
            'number',
            'tag'
        ]
