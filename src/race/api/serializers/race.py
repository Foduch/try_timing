from rest_framework import serializers

from race.models import RaceModel


class RaceSerializer(serializers.ModelSerializer):
    '''
    Класс сериализации данных из объектов python в JSON и обратно.
    '''
    class Meta:
        model  = RaceModel
        fields = [
            'guid',
            'name',
            'start_date_time',
            'race_type',
            'description',
            'region',
            'city',
            'status',
        ]
