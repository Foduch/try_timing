from rest_framework import serializers

from race.models import ParticipantModel


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParticipantModel
        fields = [
            'guid',
            'athlete',
            'group',
            'number',
            'tag',
            'description',
            'race',
        ]
