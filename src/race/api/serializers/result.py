from rest_framework import serializers

from race.models import ResultModel


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResultModel
        fields = [
            'guid',
            'participant',
            'result_time',
            'laps_count',
            'laps_time',
            'status',
        ]
