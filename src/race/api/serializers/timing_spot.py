from rest_framework import serializers

from race.models import TimingSpotModel


class TimingSpotSerializer(serializers.ModelSerializer):
    '''
    Класс сериализации данных из объектов python в JSON и обратно.
    '''
    class Meta:
        model  = TimingSpotModel
        fields = [
            'guid',
            'name',
            'race',
            'index',
            'is_lap',
            'is_finish'
        ]
