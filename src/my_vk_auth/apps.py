from django.apps import AppConfig


class MyVkAuthConfig(AppConfig):
    name = 'my_vk_auth'
