from django.conf import settings
from django.contrib.auth.models import User
import requests
from .models import MyVkToken


class MyVkBackend:

    token_url = settings.MY_VK_TOKEN_URL
    client_id = settings.MY_CLIENT_ID
    client_secret = settings.MY_CLIENT_SECRET

    def authenticate(self, request, code):
        response = requests.post(self.token_url, data={'client_id': self.client_id, 'client_secret': self.client_secret,
                                                       'code': code, 'redirect_uri': settings.MY_VK_REDIRECT_URI
                                                       }).json()
        access_token = response['access_token']
        user_id = response['user_id']

        try:
            user = User.objects.get(pk=user_id)
        except:
            user = User(id=user_id)
            user.save()

        (token, created) = MyVkToken.objects.get_or_create(user=user)
        token.token = access_token
        token.save()

        return user

    def get_user(self, user_id):

        try:
            return User.objects.get(id=user_id)
        except:
            return None
