from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from rest_framework.authtoken.models import Token


def vk_finish_auth(request):

    code = request.GET['code']
    user = authenticate(request, code=code)
    login(request, user, backend='my_vk_auth.backend.MyVkBackend')
    token = Token.objects.update_or_create(user=user)[0]
    

    return JsonResponse({'token': str(token), 'userId': str(user.id)})