from django.urls import path
from .views import vk_finish_auth

urlpatterns = [
    path('vk-login', vk_finish_auth, name='vk-login'),
]
