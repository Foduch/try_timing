try:
    from distutils.core import setup
except:
    from setuptools import setup

setup(
    name='Timing for cycling',
    version='0.1dev',
    packages=['athlete', 'constants', 'race', ],
)
