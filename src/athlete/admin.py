from django.contrib import admin

from athlete.models import AthleteModel
from athlete.forms  import AthleteForm


class AthleteAdmin(admin.ModelAdmin):
    '''
    Класс модели для настройки в панели админа.
    '''
    list_display = ['first_name', 'last_name', 'gender']
    form         = AthleteForm

# Регистрация модели в панели админа.
admin.site.register(AthleteModel, AthleteAdmin)
