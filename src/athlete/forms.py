from django import forms

from athlete.models import AthleteModel


class AthleteForm(forms.ModelForm):
    '''
    Класс формы для валидации объекта модели при добавлении и редактирования.
    Используется в панели админа.
    '''
    class Meta:
        model  = AthleteModel
        fields = ['first_name', 'last_name', 'gender', 'birthday', 'country', 'region', 'city']
