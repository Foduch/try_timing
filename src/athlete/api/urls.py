from django.urls import path

from athlete.api.views import AthleteDetailAPIView, AthleteListApiView

urlpatterns = [
    path('<uuid:pk>', AthleteDetailAPIView.as_view()),
    path('', AthleteListApiView.as_view()),
]
