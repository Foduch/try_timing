from rest_framework import serializers

from athlete.models import AthleteModel


class AthleteSerializer(serializers.ModelSerializer):
    '''
    Класс сериализации данных из объектов python в JSON и обратно.
    '''
    class Meta:
        model  = AthleteModel
        fields = [
            'guid',
            'first_name',
            'last_name', 'gender',
            'birthday',
            'country',
            'region',
            'city'
        ]

    def validate(self, data):
        pass
        return data
