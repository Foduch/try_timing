from rest_framework import generics, exceptions
from rest_framework.response import Response
from datetime import date

from athlete.api.serializers import AthleteSerializer
from athlete.models          import AthleteModel


class AthleteDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = AthleteSerializer
    queryset               = AthleteModel.objects.filter(isDeleted=False)

    def delete(self, request, *args, **kwargs):
        try:
            athlete_ = self.queryset.get(pk=self.kwargs.get('pk'))
            athlete_.delete()
            return Response(status=204)
        except exceptions.NotFound:
            return Response(status=404)
        return Response(status=400)


class AthleteListApiView(generics.ListCreateAPIView):
    permission_classes     = []
    authentication_classes = []
    serializer_class       = AthleteSerializer
    queryset               = AthleteModel.objects.filter(isDeleted=False)

    def get(self, request, *args, **kwargs):
        queryset = AthleteModel.objects.filter(isDeleted=False)
        first_name = self.request.query_params.get('first_name', None)
        last_name = self.request.query_params.get('last_name', None)
        gender = self.request.query_params.get('gender', None)
        age_min = self.request.query_params.get('age_min', None)
        age_max = self.request.query_params.get('age_max', None)
        if first_name is not None:
            queryset = queryset.filter(first_name__icontains=first_name)
        if last_name is not None:
            queryset = queryset.filter(last_name__icontains=last_name)
        if gender is not None:
            queryset = queryset.filter(gender=gender)
        if (age_min is not None) or (age_max is not None):
            year = date.today().year
        if age_min is not None:
            queryset = queryset.filter(birthday__lte=date(year - int(age_min), 12, 31))
        if age_max is not None:
            queryset = queryset.filter(birthday__gte=date(year - int(age_max), 1, 1))
        serialzier_ = self.serializer_class(queryset, many=True)
        return Response(data=serialzier_.data, status=200)
