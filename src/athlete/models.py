from django.db import models
import uuid

from constants.choices import gender


class AthleteQuerySet(models.QuerySet):
    pass


class AthleteManager(models.Manager):
    def get_queryset(self):
        return AthleteQuerySet(self.model, using=self._db)


class AthleteModel(models.Model):
    '''
    Класс модели спортсмена для БД.
    '''
    guid          = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name    = models.CharField(max_length=128)
    last_name     = models.CharField(max_length=128)
    gender        = models.CharField(max_length=20, choices=gender)
    birthday      = models.DateField()
    country       = models.CharField(max_length=100, default='russia')
    region        = models.CharField(max_length=100, blank=True)
    city          = models.CharField(max_length=100, blank=True)
    isDeleted     = models.BooleanField(default=False)

    def __str__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    class Meta:
        # Отображаемые имена для экземпляра и списка объектов модели.
        verbose_name        = 'Athlete'
        verbose_name_plural = 'Athletes'
